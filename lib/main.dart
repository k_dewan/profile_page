import 'package:flutter/material.dart';
import 'package:profile_page_1/pages/blogs_videos/blogs.dart';
import 'package:profile_page_1/pages/blogs_videos/blogs_desc.dart';
import 'package:profile_page_1/pages/blogs_videos/videos.dart';
import 'package:profile_page_1/pages/booking_history/appointment.dart';
import 'package:profile_page_1/pages/booking_consultation/cdawaited.dart';
import 'package:profile_page_1/pages/booking_consultation/bdcompleted.dart';
import 'package:profile_page_1/pages/booking_consultation/cdcompleted.dart';
import 'package:profile_page_1/pages/booking_history/booking_history.dart';
import 'package:profile_page_1/pages/location.dart';
import 'package:profile_page_1/pages/payment/payment.dart';
import 'package:profile_page_1/widgets/test_button.dart';
import 'package:profile_page_1/widgets/review_desc.dart';
import 'package:profile_page_1/widgets/review_star.dart';
import 'package:profile_page_1/pages/profilepage.dart';
import 'package:profile_page_1/pages/profilepage_2.dart';
import 'package:profile_page_1/pages/review.dart';
import 'package:profile_page_1/pages/testservices.dart';
import 'package:profile_page_1/pages/testcarousel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile Ui',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Payment(),
    );
  }
}