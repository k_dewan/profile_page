// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class TestServices extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  TestServices({Key? key}) : super(key: key);

  @override
  State<TestServices> createState() => _TestServicesState();
}

class _TestServicesState extends State<TestServices> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        backgroundColor: Color(0XFF60C6D5),
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 672,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.01, 10],
                    colors: [Color(0XFF60C6D5), Colors.white60])),
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(bottom: 8),
                            child: const Text(
                              'Our',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Text(
                            'Services...!',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/images/bg.png'))),
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.all(15)),
                InkWell(
                  child: Card(
                      child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Row(
                      children: [
                        // ignore: sized_box_for_whitespace
                        Container(
                          width: 100.0,
                          height: 70.0,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(96, 198, 213, 0.5),
                                    blurRadius: 20.0,
                                    spreadRadius: 5.0,
                                  )
                                ],
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/service_item.png'),
                                )),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: const Text(
                                  'ACNE TREATMENT',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              Text(
                                'Acne is the most common skin disease that patients complain...',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey[500],
                                ),
                              ),
                            ],
                          ),
                        ),
                        FloatingActionButton(
                          onPressed: () {},
                          backgroundColor: Color.fromRGBO(96, 198, 213, 1),
                          child: Icon(Icons.arrow_forward),
                          mini: true,
                        )
                      ],
                    ),
                  )),
                  onTap: () {},
                ),
                InkWell(
                  child: Card(
                      child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Row(
                      children: [
                        // ignore: sized_box_for_whitespace
                        Container(
                          width: 100.0,
                          height: 70.0,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(96, 198, 213, 0.5),
                                    blurRadius: 20.0,
                                    spreadRadius: 5.0,
                                  )
                                ],
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/service_item.png'),
                                )),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: const Text(
                                  'ACNE TREATMENT',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              Text(
                                'Acne is the most common skin disease that patients complain...',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey[500],
                                ),
                              ),
                            ],
                          ),
                        ),
                        FloatingActionButton(
                          onPressed: () {},
                          backgroundColor: Color.fromRGBO(96, 198, 213, 1),
                          child: Icon(Icons.arrow_forward),
                          mini: true,
                        )
                      ],
                    ),
                  )),
                  onTap: () {},
                ),
                InkWell(
                  child: Card(
                      child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Row(
                      children: [
                        // ignore: sized_box_for_whitespace
                        Container(
                          width: 100.0,
                          height: 70.0,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(96, 198, 213, 0.5),
                                    blurRadius: 20.0,
                                    spreadRadius: 5.0,
                                  )
                                ],
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/service_item.png'),
                                )),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: const Text(
                                  'ACNE TREATMENT',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              Text(
                                'Acne is the most common skin disease that patients complain...',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey[500],
                                ),
                              ),
                            ],
                          ),
                        ),
                        FloatingActionButton(
                          onPressed: () {},
                          backgroundColor: Color.fromRGBO(96, 198, 213, 1),
                          child: Icon(Icons.arrow_forward),
                          mini: true,
                        )
                      ],
                    ),
                  )),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.white.withOpacity(0.5),
        buttonBackgroundColor: const Color(0XFF60C6D5),
        height: MediaQuery.of(context).size.height * 0.085,
        items: <Widget>[
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/home.png',
                  fit: BoxFit.fill,
                  color:
                      _currentIndex == 0 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text(
                "Home",
                style: TextStyle(
                  fontSize: 9,
                  color:
                      _currentIndex == 0 ? Colors.white : Colors.grey.shade400,
                ),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/caduceus.png',
                  fit: BoxFit.fill,
                  color:
                      _currentIndex == 1 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text(
                "Services",
                style: TextStyle(
                  fontSize: 9,
                  color:
                      _currentIndex == 1 ? Colors.white : Colors.grey.shade400,
                ),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/my_appt.png',
                  fit: BoxFit.fill,
                  color:
                      _currentIndex == 2 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text(
                "My Appt.",
                style: TextStyle(
                  fontSize: 9,
                  color:
                      _currentIndex == 2 ? Colors.white : Colors.grey.shade400,
                ),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/pay_now.png',
                  fit: BoxFit.fill,
                  color:
                      _currentIndex == 3 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text(
                "Pay Now",
                style: TextStyle(
                  fontSize: 9,
                  color:
                      _currentIndex == 3 ? Colors.white : Colors.grey.shade400,
                ),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/profile.png',
                  fit: BoxFit.fill,
                  color:
                      _currentIndex == 4 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text(
                "Profile",
                style: TextStyle(
                  fontSize: 9,
                  color:
                      _currentIndex == 4 ? Colors.white : Colors.grey.shade400,
                ),
              ),
            ],
          ),
        ],
        onTap: (index) {
          //Handle button tap
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
