import 'package:flutter/material.dart';
import 'package:profile_page_1/widgets/gender.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();
  String dropdownValue = 'Male';

  bool _decideWhichDayToEnable(DateTime day) {
  if ((day.isAfter(DateTime.now().subtract(Duration(days: 1))) &&
      day.isBefore(DateTime.now().add(Duration(days: 0))))) {
    return true;
  }
  return false;
}

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1930),
      lastDate: DateTime(2021),
      selectableDayPredicate: _decideWhichDayToEnable,
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        
        backgroundColor: Color.fromRGBO(96, 198, 213, 1),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        title: const Text('My Profile'),
        
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 600,
                width: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      const Padding(padding: EdgeInsets.all(20)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Full Name", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: "Eg. John",
                          border: OutlineInputBorder()
                        ),                  
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Email ID", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: "Eg. john@gmail.com",
                          border: OutlineInputBorder()
                        ),                  
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Date of Birth", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 55,
                      
                        child: OutlinedButton(
                          
                          onPressed: () => _selectDate(context),
                  
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0))),
                              
                            ),
                            child: 
                              Align(
                                alignment: Alignment.center,
                                child: const Text('DD/MM/YYYY', style: TextStyle(
                                color: Colors.black45,
                                
                            )),
                              ),
                            
                          ),
                          
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Gender", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      
                      Gender(),  


                       const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Address", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: "Eg. New Delhi",
                          border: OutlineInputBorder()
                        ),                  
                      ),
                      const Padding(padding: EdgeInsets.all(10)),


                    
                      // ignore: sized_box_for_whitespace
                      Container(
                        height: 55,
                        width: double.infinity,
                        child: ElevatedButton(
                          child: const Center(
                            child: 
                            Text('SAVE'),),
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            primary: const Color.fromRGBO(96, 198, 213, 1),
                            textStyle: const TextStyle(
                              fontSize: 18,
                              color: Colors.white,)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          CustomPaint(
            // ignore: sized_box_for_whitespace
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderContainer(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              
              const Padding(
                padding: EdgeInsets.all(7),
              ),
              
              Container(
                padding: const EdgeInsets.all(5.0),
                width: MediaQuery.of(context).size.width / 3,
                height: MediaQuery.of(context).size.width / 3,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 5),
                  shape: BoxShape.circle,
                  color: Colors.white,
                  image: const DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/profilepicture.jpg'),
                  ),
                ),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(96, 198, 213, 1),
                        borderRadius:BorderRadius.circular(100)
                    ),                    
                    child: Container(
                      child: InkWell(
                        child: const Icon(
                          Icons.edit, 
                          size: 14.0, 
                          color: Colors.white
                        ),
                        onTap: () {},
                        ),
                    ),
                  ),
                )
              ),
            ],
          ),
          
        ],
      ),
    );
  }
}

class HeaderContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = const Color.fromRGBO(96, 198, 213, 1);
    Path path = Path()
      ..relativeLineTo(0, 85)
      ..relativeLineTo(size.width, 0)
      ..relativeLineTo(0, -85)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

List<String> gender = [
  "Female",
  "Male",
  "Other",
];