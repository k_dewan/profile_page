// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class TestCarousel extends StatefulWidget {
  const TestCarousel({Key? key}) : super(key: key);

  @override
  _TestCarouselState createState() => _TestCarouselState();
}

class _TestCarouselState extends State<TestCarousel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        backgroundColor: Color(0XFF60C6D5),
        elevation: 0.0,
      ),
      body: Stack(
        //alignment: Alignment.center,
        children: [
          CustomPaint(
            // ignore: sized_box_for_whitespace
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderContainer(),
          ),
          Column(
            children: [
              Row(
                children: [
                  Expanded(                      
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [                          
                        Container(
                          padding: const EdgeInsets.only(bottom: 8, left: 20),
                          child: const Text(
                            'Our',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: Text(
                            'Services...!',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/images/bg.png'))),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 25, horizontal: 0),
                width: 360.0,
                height: 450.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      offset: Offset(0, 3),
                      blurRadius: 7,
                      spreadRadius: 5,
                    )
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        height: 200.0,
                        width: double.infinity,
                        child: Carousel(
                          images: [                            
                            AssetImage('assets/images/before.jpg'),                            
                            AssetImage('assets/images/before.jpg'),                            
                            AssetImage('assets/images/before.jpg'),                            
                            AssetImage('assets/images/before.jpg'),
                          ],
                          animationCurve: Curves.linear,
                          dotColor: Colors.grey,
                          dotSize: 5.0,
                          dotSpacing: 15.0,
                          boxFit: BoxFit.cover,
                          dotBgColor: Colors.transparent,
                        )),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Acne Treatment in Gurgaon",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "La Skinnovita, is a renowned",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ),
                            ),
                            TextSpan(
                              text: " skin and hair clinic in Gurgaon",
                              style: TextStyle(
                                color: Color(0XFF60C6D5),
                                fontSize: 15.0,
                              ),
                            ),
                            TextSpan(
                              text: ". According to the skin types and texture, Dr. Anuj Pall and his team of expert dermatologists proficiently guide you with the most efficient and effective Acne treatment in Gurgaon at La Skinnovita.",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ),
                            ),
                          ]
                        )
                       
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(10),
                        ),
                    TextButton(
                        onPressed: () {},
                        child: Text("View More Details",
                            style: TextStyle(
                                color: Color(0XFF60C6D5),
                                fontSize: 18,
                                fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: TextButton(
              onPressed: () {},
              style: TextButton.styleFrom(
                primary: Colors.white,
              ),
              child: Text(
                "Book Appointment",
                textAlign: TextAlign.end,
                style: TextStyle(
                    color: Color(0XFF60C6D5),
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class HeaderContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = const Color.fromRGBO(96, 198, 213, 1);
    Path path = Path()
      ..relativeLineTo(0, 200)
      ..relativeLineTo(size.width, 0)
      ..relativeLineTo(0, -200)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
