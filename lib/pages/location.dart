// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors
//add pop up menu
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


class Location extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  Location({Key? key}) : super(key: key);

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  final items = <Widget>[
    ListTile(
      leading: Icon(Icons.home_filled, color: Colors.black),
      title: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: const Text(
                    'Home',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'A-601, Janakpuri West, New Delhi, 110058',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            color: Color(0XFF60C6D5),
            onPressed: () {}, //add menu here
          ),
        ],
      ),
      onTap: () {},
    ),
    ListTile(
      leading: Icon(Icons.home_filled, color: Colors.black),
      title: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: const Text(
                    'Home',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'A-601, Janakpuri West, New Delhi, 110058',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            color: Color(0XFF60C6D5),
            onPressed: () {}, //add menu here
          ),
        ],
      ),
      onTap: () {},
    ),
    Divider(
      thickness: 5,
    ),
  ];

  final itemsList = <Widget>[
    ListTile(
      leading: Icon(Icons.timelapse_rounded, color: Colors.grey),
      title: Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Text(
          'A-601, Janakpuri West, New Delhi, 110058',
          style: TextStyle(
            color: Colors.grey[500],
          ),
        ),
      ),
      onTap: () {},
    ),
    Divider(
      thickness: 2,
      endIndent: 20,
      indent: 20,
    ),
    ListTile(
      leading: Icon(
        Icons.timelapse_rounded,
        color: Colors.grey,
      ),
      title: Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Text(
          'A-601, Janakpuri West, New Delhi, 110058',
          style: TextStyle(
            color: Colors.grey[500],
          ),
        ),
      ),
      onTap: () {},
    ),
    Divider(
      thickness: 2,
      endIndent: 20,
      indent: 20,
    ),
  ];

  final TextEditingController _Textcontroller = TextEditingController();



  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.23,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
              color: Color(0XFF60C6D5),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Set your location',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.all(15.0),
                  padding: const EdgeInsets.all(0.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Row(children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(0.0),
                          hintText: 'Search for your location',
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 17.0,
                          ),
                          border: InputBorder.none,
                        ),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                        ),
                      ),
                    ),
                  ]),
                ),
                Container(
                  margin:
                      const EdgeInsets.only(left: 15.0, bottom: 8, right: 15),
                  padding: const EdgeInsets.all(0.0),
                  child: Row(children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Icon(
                        Icons.location_searching_sharp,
                        color: Colors.white,
                      ),
                    ),
                    Flexible(
                      child: TextButton(
                        child: Text(
                          'Use Current Location',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17.0,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Saved Addresses",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold),
                )),
          ),
          
          Wrap(
            children: items,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Recent Location",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold),
                )),
          ),
          Wrap(
            children: itemsList,
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.03,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              width: double.infinity,
              height: double.infinity,
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.99,
                maxHeight: MediaQuery.of(context).size.height * 0.065,
              ),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                color: Color(0XFF60C6D5),
                onPressed: () {},
                child: Text(
                  'CONTINUE',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17.0,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.01,
          ),
        ],
      ),
    );
  }
}
