import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:profile_page_1/widgets/gender.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        
        backgroundColor: Color.fromRGBO(96, 198, 213, 1),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        title: const Text('My Profile'),
        actions: [
          Icon(Icons.edit), 
          Container(padding: const EdgeInsets.only(left: 5.0, top:20.0, bottom:20.0, right:20.0), child: const Text("Edit", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),)),
        ],
        
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 500,
                width: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 10),
                
                child: Container(
                  
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: <Widget>[
                      const Align(
                        alignment: Alignment.center,
                        child: Text("DAVID WARNER", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 22,
                          ),
                        ),
                      ),
                      
                      const Padding(padding: EdgeInsets.all(20)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Email", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(3)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("david_warner_aussi@gmail.com", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                        color: Colors.black54,
                          ),
                        ),
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Gender", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(3)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Male", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                        color: Colors.black54,
                          ),
                        ),
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Date of Birth", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(3)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("27 October 1986", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                        color: Colors.black54,
                          ),
                        ),
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Registered Mobile", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(3)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("+91-9876543210", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                        color: Colors.black54,
                          ),
                        ),
                      ),


                      const Padding(padding: EdgeInsets.all(10)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Address", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(3)),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Janakpuri West, New Delhi, 110056", style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 18,
                        color: Colors.black54,
                          ),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.all(10)),


                    
                      // ignore: sized_box_for_whitespace
                      
                    ],
                  ),
                ),
              )
            ],
          ),
          CustomPaint(
            // ignore: sized_box_for_whitespace
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderCurvedContainer(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              
              const Padding(
                padding: EdgeInsets.all(7),
              ),
              
              Container(
                padding: const EdgeInsets.all(5.0),
                width: MediaQuery.of(context).size.width / 3,
                height: MediaQuery.of(context).size.width / 3,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 5),
                  shape: BoxShape.circle,
                  color: Colors.white,
                  image: const DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/profile.jpg'),
                  ),
                ),
              
              ),
            ],
          ),
          
        ],
      ),
      
      bottomNavigationBar: CurvedNavigationBar(
       
        backgroundColor: Colors.white.withOpacity(0.5),
        buttonBackgroundColor: const Color(0XFF60C6D5),
        height: MediaQuery.of(context).size.height * 0.085,
        items: <Widget>[
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/home.png',
                  fit: BoxFit.fill,
                  color: _currentIndex == 0 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text("Home", 
              style: TextStyle(fontSize: 9,
              color: _currentIndex == 0 ? Colors.white : Colors.grey.shade400,),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/caduceus.png',
                  fit: BoxFit.fill,
                  color: _currentIndex == 1 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text("Services", 
              style: TextStyle(fontSize: 9,
              color: _currentIndex == 1 ? Colors.white : Colors.grey.shade400,),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/my_appt.png',
                  fit: BoxFit.fill,
                  color: _currentIndex == 2 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text("My Appt.", 
              style: TextStyle(fontSize: 9,
              color: _currentIndex == 2 ? Colors.white : Colors.grey.shade400,),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/pay_now.png',
                  fit: BoxFit.fill,
                  color: _currentIndex == 3 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text("Pay Now", 
              style: TextStyle(fontSize: 9,
              color: _currentIndex == 3 ? Colors.white : Colors.grey.shade400,),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.050,
                width: MediaQuery.of(context).size.width * 0.090,
                padding: EdgeInsets.all(4.0),
                child: Image.asset(
                  'assets/images/profile.png',
                  fit: BoxFit.fill,
                  color: _currentIndex == 4 ? Colors.white : Colors.grey.shade400,
                ),
              ),
              Text("Profile", 
              style: TextStyle(fontSize: 9,
              color: _currentIndex == 4 ? Colors.white : Colors.grey.shade400,),
              ),
            ],
          ),
        ],
        onTap: (index) {
          //Handle button tap
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}


class HeaderCurvedContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = const Color.fromRGBO(96, 198, 213, 1);
    Path path = Path()
      ..relativeLineTo(0, 85)
      ..relativeLineTo(size.width, 0)
      ..relativeLineTo(0, -85)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}