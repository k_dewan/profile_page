// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:profile_page_1/pages/booking_history/appointment_desc.dart';
import 'package:profile_page_1/pages/booking_history/consultation_desc.dart';

class ConsultationCards extends StatefulWidget {
  const ConsultationCards({Key? key}) : super(key: key);

  @override
  _ConsultationCardsState createState() => _ConsultationCardsState();
}

class _ConsultationCardsState extends State<ConsultationCards> {
  
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
                children: [
                  
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 20, bottom: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Upcoming Online Consultation",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  ConsultationDesc(),
                  
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  ConsultationDesc(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  Divider(
                    thickness: 4,
                    
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 20, bottom: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Online Consultation History",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  ConsultationDescAwaited(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  ConsultationDescCancelled(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  ConsultationDescCancelled(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  ConsultationDescCancelled(),SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  ConsultationDescCancelled(),
    
                        
        
                ],
                        ),
    );
  }
}









class AppointmentCards extends StatefulWidget {
  const AppointmentCards({Key? key}) : super(key: key);

  @override
  _AppointmentCardsState createState() => _AppointmentCardsState();
}

class _AppointmentCardsState extends State<AppointmentCards> {
  
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
                children: [
                  
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 20, bottom: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Upcoming Appointment",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  AppointmentDesc(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  AppointmentDesc(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  Divider(
                    thickness: 4,
                    
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 20, bottom: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Appointment History",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  AppointmentDescAwaited(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  AppointmentDescCancelled(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  AppointmentDescCancelled(),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  AppointmentDescCancelled(),SizedBox(height: MediaQuery.of(context).size.height * 0.007),
                  AppointmentDescCancelled(),
    
                        
        
                ],
                        ),
    );
  }
}