// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:profile_page_1/pages/booking_consultation/bdawaited.dart';
import 'package:profile_page_1/pages/booking_consultation/bdcancelled.dart';
import 'package:profile_page_1/pages/booking_consultation/bdcompleted.dart';

class AppointmentDesc extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  AppointmentDesc({Key? key}) : super(key: key);

  @override
  _AppointmentDescState createState() => _AppointmentDescState();
}

class _AppointmentDescState extends State<AppointmentDesc> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
          color: Color.fromRGBO(231, 255, 229, 1),
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.09,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // ignore: sized_box_for_whitespace
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, bottom: 8, top: 15),
                          child: Text(
                            'Booking Id: DEL123456',
                            style: TextStyle(
                              //fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0, left: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.circle,
                                size: 11,
                                color: Colors.grey,
                              ),
                              RichText(
                                  text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                  text: "  25/02/2021 |",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                  ),
                                ),
                                TextSpan(
                                  text: " Confirmed",
                                  style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ])),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BookingDetailsCompleted()),
                        );
                      },
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: Color(0XFF60C6D5),
                      )),
                ],
              ),
            ),
          )),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BookingDetailsCompleted()),
        );
      },
    );
  }
}

class AppointmentDescAwaited extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  AppointmentDescAwaited({Key? key}) : super(key: key);

  @override
  _AppointmentDescAwaitedState createState() => _AppointmentDescAwaitedState();
}

class _AppointmentDescAwaitedState extends State<AppointmentDescAwaited> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.09,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // ignore: sized_box_for_whitespace
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, bottom: 8, top: 15),
                          child: Text(
                            'Booking Id: DEL123456',
                            style: TextStyle(
                              //fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0, left: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.circle,
                                size: 11,
                                color: Colors.grey,
                              ),
                              RichText(
                                  text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                  text: "  25/02/2021 |",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                  ),
                                ),
                                TextSpan(
                                  text: " Awaited",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ])),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BookingDetailsAwaited()),
                        );
                      },
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: Color(0XFF60C6D5),
                      )),
                ],
              ),
            ),
          )),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BookingDetailsAwaited()),
        );
      },
    );
  }
}

class AppointmentDescCancelled extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  AppointmentDescCancelled({Key? key}) : super(key: key);

  @override
  _AppointmentDescCancelledState createState() =>
      _AppointmentDescCancelledState();
}

class _AppointmentDescCancelledState extends State<AppointmentDescCancelled> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
          color: Color.fromRGBO(255, 245, 245, 1),
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.09,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // ignore: sized_box_for_whitespace
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, bottom: 8, top: 15),
                          child: Text(
                            'Booking Id: DEL123456',
                            style: TextStyle(
                              //fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0, left: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.circle,
                                size: 11,
                                color: Colors.grey,
                              ),
                              RichText(
                                  text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                  text: "  25/02/2021 |",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                  ),
                                ),
                                TextSpan(
                                  text: " Cancelled",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ])),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BookingDetailsCancelled()),
                        );
                      },
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: Color(0XFF60C6D5),
                      )),
                ],
              ),
            ),
          )),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BookingDetailsCancelled()),
        );
      },
    );
  }
}
