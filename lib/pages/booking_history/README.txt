From the flow of 34 PAGES:

page 8 & 9: booking_history.dart (don't forget to import other files of this folder)

This folder contains all the files of the Appointment History.


You can place all these files into one folder and these files will be working perfectly.
So, the file booking_history.dart is the main file which calls other 3 files present in the 
folder. Since, things are static, different cards are made for three purposes: Confirmed, 
Awaited, Cancelled. 
For Booking Appointment, the format of these three cards is stored in appointment_desc.dart. Similarly,
for Consultation, the format of these three cards are stored in consultation_desc.dart. 
appointment.dart is just calling all these cards at one place as AppointmentCards() and
ConsultationCards(). These collections of cards are being called in booking_history.dart.