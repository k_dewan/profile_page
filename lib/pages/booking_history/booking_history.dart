// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:profile_page_1/pages/booking_history/appointment.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        backgroundColor: Color(0XFF60C6D5),
        elevation: 0.0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 10,
            child: Container(
              padding: const EdgeInsets.only(left: 32, top: 0, right: 32),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: const Text(
                            'My',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Text(
                          'Appointment History...!',
                          style: TextStyle(
                            fontSize: 26,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    //height: MediaQuery.of(context).size.height * 0.08,
                    padding: EdgeInsets.only(bottom: 28.0),
                    child: Image.asset(
                      'assets/images/appointment_history.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            ),
            color: Color(0XFF60C6D5),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
            child: TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                    side: BorderSide(color: Color(0XFF60C6D5))),
                color: Color(0xffF7FEFF),
              ),
              indicatorColor: Color(0XFF60C6D5),
              unselectedLabelColor: Color(0XFF60C6D5),
              labelColor: Color(0XFF60C6D5),
              tabs: [
                Tab(
                  text: 'Appointment',
                ),
                Tab(
                  text: 'Online Consultation',
                )
              ],
              controller: _tabController,
            ),
          ),
          Expanded(
            child: TabBarView(
              children: [
                AppointmentCards(),
                ConsultationCards(),
              ],
              controller: _tabController,
            ),
          ),
        ],
      ),
    );
  }
}


//indicator: BubbleTabIndicator(
//                    indicatorHeight: 40.0,
//                    indicatorColor: Color(0xffF7FEFF),
//                    tabBarIndicatorSize: TabBarIndicatorSize.tab,
//                    // Other flags
//                    indicatorRadius: 20,
//                    insets: EdgeInsets.all(1),
//                    padding: EdgeInsets.all(10)),