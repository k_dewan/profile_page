// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors
//PAGE 19
import 'package:flutter/material.dart';

class BlogsDescription extends StatefulWidget {
  const BlogsDescription({Key? key}) : super(key: key);

  @override
  _BlogsDescriptionState createState() => _BlogsDescriptionState();
}

class _BlogsDescriptionState extends State<BlogsDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(0XFF60C6D5),
        elevation: 0.0,
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.01, 10],
                    colors: [Color(0XFF60C6D5), Colors.white60])),
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: InkWell(
                child: Card(
                  elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                          child: Container(
                            padding: const EdgeInsets.only(bottom: 8),
                            child: const Text(
                              'Hair Transplant an effective way to Clear Away the Baldness',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 170,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/hair_transplant.png'),
                                  fit: BoxFit.fill)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, top: 20, right: 15),
                          child: Text(
                            'An exceptionally entertaining statement from somebody "I don\'t believe myself to be bare, I am only taller than my hair!" This is the means by which'
                            ' individuals decide and support themselves when they lose the entirety of their. Youth age and great thich hair are two things which'
                            ' we take as in truth, until they are gone from us. Keep in mind, the breezes love to play with your exposed feet. Thus, '
                            'it is consistently fitting to take generally excellent consideration of your hair. However, in the event that you are as of now finished with '
                            'hair sparseness; at that point it is smarter to go for a careful procedure.',
                            style: TextStyle(
                              height: 1.3,
                              fontSize: 18,
                              color: Colors.grey[500],
                              fontWeight: FontWeight.w500,            
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, top: 8, right: 15),
                          child: Text(
                            'An exceptionally entertaining statement from somebody "I don\'t believe myself to be bare, I am only taller than my hair!" This is the means by which',
                            style: TextStyle(
                              height: 1.3,
                              fontSize: 18,
                              color: Colors.grey[500],
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, top: 8, right: 15),
                          child: Text(
                            'However, in the event that you are as of now finished with '
                            'hair sparseness; at that point it is smarter to go for a careful procedure '
                            'called transplant, that includes moving individual follicles from one aspect '
                            'of the body(the giver site) to',
                            style: TextStyle(
                              height: 1.3,
                              fontSize: 18,
                              color: Colors.grey[500],
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, top: 8, bottom: 8),
                          child: Text(
                            '1 day ago',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[500],
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ],
                    )),
                onTap: () {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}