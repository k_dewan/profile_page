// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Videos extends StatefulWidget {
  @override
  _VideosState createState() => _VideosState();
}

class _VideosState extends State<Videos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        backgroundColor: Color(0XFF60C6D5),
        elevation: 0.0,
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,            
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.01, 10],
                    colors: [Color(0XFF60C6D5), Colors.white60])),
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(bottom: 8),
                              child: const Text(
                                'Latest',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Text(
                              'Videos...!',
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage('assets/images/bg.png'))),
                      )
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.035),
                  //Padding(padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10)),
                  InkWell(
                    child: Card(
                      elevation: 6,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              
                              child: Align(
                                  alignment: Alignment.center,
                                  child: IconButton(
                                    iconSize: 50,
                                    icon: Icon(Icons.play_circle_fill_sharp),
                                    color: Color(0XFF60C6D5),
                                    onPressed: () {},
                                  )),
                            ),
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  colorFilter: ColorFilter.mode(
                                      Colors.black.withOpacity(0.7),
                                      BlendMode.dstATop),
                                  image: AssetImage("assets/images/watch_video.png"),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: 12, bottom: 15),
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    "Laser Therapy for acne removals...",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                    onTap: () {},
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                  InkWell(
                    child: Card(
                      elevation: 6,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              
                              child: Align(
                                  alignment: Alignment.center,
                                  child: IconButton(
                                    iconSize: 50,
                                    icon: Icon(Icons.play_circle_fill_sharp),
                                    color: Color(0XFF60C6D5),
                                    onPressed: () {},
                                  )),
                            ),
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  colorFilter: ColorFilter.mode(
                                      Colors.brown.withOpacity(0.7),
                                      BlendMode.dstATop),
                                  image: AssetImage("assets/images/watch_video.png"),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: 12, bottom: 15),
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    "Laser Therapy for acne removals...",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                    onTap: () {},
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                  InkWell(
                    child: Card(
                      elevation: 6,                      
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Stack(
                          children: [
                            Positioned.fill(                              
                              child: Align(
                                  alignment: Alignment.center,
                                  child: IconButton(
                                    iconSize: 50,
                                    icon: Icon(Icons.play_circle_fill_sharp),
                                    color: Color(0XFF60C6D5),
                                    onPressed: () {},
                                  )),
                            ),
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  colorFilter: ColorFilter.mode(
                                      Colors.brown.withOpacity(0.7),
                                      BlendMode.dstATop),
                                  image: AssetImage("assets/images/watch_video.png"),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left: 12, bottom: 15),
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    "Laser Therapy for acne removals...",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                    onTap: () {},
                  ),
                  
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


