// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class ReviewStar extends StatefulWidget {
  ReviewStar({Key? key}) : super(key: key);

  @override
  _ReviewStarState createState() => _ReviewStarState();
}

class _ReviewStarState extends State<ReviewStar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25,
      width: 50,
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 249, 214, 1),
        borderRadius: BorderRadius.circular(3),
      ),
      child: Row(
        children: [
          SizedBox(
            height: 8,
            width: 3,
          ),
          Icon(
            Icons.star,
            color: Color.fromRGBO(245, 173, 71, 1),
            size: 15,
          ),
          SizedBox(
            height: 8,
            width: 8,
          ),
          Text(
            "4.9",
            style: TextStyle(
              color: Color.fromRGBO(245, 173, 71, 1),
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
