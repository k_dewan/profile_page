// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:profile_page_1/widgets/review_star.dart';

class ReviewDesc extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  ReviewDesc({Key? key}) : super(key: key);

  @override
  _ReviewDescState createState() => _ReviewDescState();
}

class _ReviewDescState extends State<ReviewDesc> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Card(
            child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // ignore: sized_box_for_whitespace
                  Container(
                    width: 60,
                    height: 60,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          
                          image: DecorationImage(
                            image: AssetImage('assets/images/profilepicture.jpg'),
                            fit: BoxFit.cover,
                          )),
                    ),
                  ),

                  // ignore: sized_box_for_whitespace
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8, left: 15),
                          child: Text('Nikita Jain',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8, left: 15),
                          child: Text('1 day ago',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[500],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  
                  ReviewStar(),
                ],
              ),
              Padding(padding: EdgeInsets.all(5)),
              Text('Dr. Anuj Pall is the best dermatologist in Gurgaon. He is a saviour in case of any skin related issues.',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 15.0,
              ),)
            ],
          ),
        )),
        onTap: () {},
      );    
  }
}
