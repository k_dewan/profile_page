// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class Gender extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Row(
          children: <Widget>[
            
            SizedBox(
              width: 70.0,
            ),
            CircleAvatar(
              backgroundColor: Colors.blue[50],
              child: Icon(Icons.face, color: Colors.grey),
            ),
            SizedBox(
              width: 10.0,
            ),
            Container(
              width: 70.0,
              child: Text(
                "Male",
                textAlign: TextAlign.left,
              ),
            ),
            CircleAvatar(
              backgroundColor: Colors.blue[50],
              child: Icon(
                Icons.face,
                color: Colors.grey,
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Container(
              width: 140.0,
              child: Text(
                "Female",
                textAlign: TextAlign.left,
              ),
            ),
          ],
        );
      },
    );
  }
}