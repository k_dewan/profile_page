// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:profile_page_1/pages/location.dart';

class TestButton extends StatelessWidget {
  const TestButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      
            alignment: Alignment.center,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: TextButton.styleFrom(
                    backgroundColor: Color(0XFF60C6D5),
                    padding: EdgeInsets.only(
                        left: 60, right: 60, top: 15, bottom: 15)),
                child: Text(
                  "CONFIRM",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                onPressed: () {
                  showModalBottomSheet(
                    elevation: 2,
                    //enableDrag: false,
                    //isScrollControlled:true,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0),
                      ),
                    ),
                    backgroundColor: Colors.white,
                    context: context,
                    builder: (context) => StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                        return Location();                                      //change file name here. It is just a test button for location. add this                    
                      },                                                        //button where you want location menu to pop up.
                    ),
                    isScrollControlled: true,
                  );
                },
              ),
            ),      
    );
  }
}